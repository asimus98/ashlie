<?php
return [
    'ERROR' => 'Что-то пошло не так :(',

    'ERROR_EMAIL' => 'Некорректный e-mail',
    'EMPTY_FIELDS' => 'Заполните все поля!',
    'ERROR_EMPTY_EMAIL' => 'Поле "Email" обязательно для заполнения.',

    'WRONG_AUTH_DATA' => 'Неправильный e-mail или пароль.',
    'ERROR_AUTH' => 'Ошибка авторизации',
    'SUCCESS_AUTH' => 'Вы успешно авторизованы.',

    'SUCCESS_REGISTARTION' => 'Вы успешно зарегистрированы.',
    'ERROR_ISSET' => 'Пользователь с таким email уже существует',
    'ERROR_REGISTRATION' => 'Ошибка регистрации',

    'EMPTY_TELEGRAM_URL' => 'Введите ссылку Telegram канала',
    'EMPTY_TELEGRAM_TITLE' => 'Введите название Telegram канала',
    'ISSET_TELEGRAM_CHANNEL' => 'Ошибка добавления! Telegram канал уже добавлен',
    'SUCCESS_ADDED_TELEGRAM' => 'Telegram канал успешно добавлен',

    'EMPTY_RSS_URL' => 'Введите ссылку на сайт.',
    'EMPTY_RSS_CHANNEL_URL' => '',
    'NO_SELECT_TELEGRAM' => 'Не выбран Telegram канал',
    'ERROR_ADDED_RSS_SITE' => 'Ошибка добавления RSS канала',
    'SUCCESS_ADDED_RSS_SITE' => 'RSS канал успешно добавлен',

    'EMPTY_RSS_URL_CHANNEL' => 'Введите ссылку на RSS канал.',
    'ERROR_ISSET_RSS_CHANNEL' => 'Такой RSS канал уже существует',
    'ERROR_RSS_CHANNEL_URL' => 'Неправильная ссылка RSS канала',
    'ERROR_REMOVE_RSS' => 'Ошибка удаления RSS канала',
    'ERROR_RSS_NOT_FOUND' => 'Ошибка! RSS канал не найден',


    'SUCCESS_ADDED_RSS_CHANNEL' => 'RSS канал успешно добавлен ',
    'SUCCESS_REMOVE_RSS' => 'RSS канал успешно удален.',


    'ACCESS_DENIED' => 'Ошибка доступа.',

    'ERROR_CAPTCHA_SESSION' => 'Ошибка генерации капчи.',
    'ERROR_ENTER_CAPTCHA' => 'Введите капчу!',

    'ERROR_USER_NOT_FOUND' => 'Пользователь с таким email адресом не найден.',
    'ERROR_RESTORE_PASSWORD' => 'Ошибка восстановления пароля.',
    'SUCCESS_RESTORE_PASSWORD' => 'Пароль успешно изменен. На Ваш Email отправлен новый пароль.',
    'RESTORE_PASSWORD' => 'Восстановление пароля',
    'MSG_RESTORE_PASSWORD' => 'Ваш новый пароль: ',


];