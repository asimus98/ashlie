<?php
/**
 * @project: Ashlie
 * @author M.Salimkhanov
 * @date:  -------------
 */

namespace Ashlie\Models;

use yii\db\ActiveRecord;

class Ashlie extends ActiveRecord {


    public static function getDb(){
        return \Yii::$app->ashlie;
    }


    public static function byId($id){
        return self::findOne(['id' => $id]);
    }
}