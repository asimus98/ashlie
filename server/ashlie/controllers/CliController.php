<?php
/**
 * @project: www
 * @author Marsel Salimkhanov 2016
 * http://asimuss.ru
 */

namespace Ashlie\Controllers;

use yii\console\Controller;

class CliController extends Controller {

    public function afterAction($event,$result){

        $log_path = \Yii::$app->basePath."/runtime/cli/";
        if(!file_exists($log_path) || !is_dir($log_path)) mkdir($log_path);

        ob_start();
        echo "---- REQUEST ----\n";
        print_r($_REQUEST);

        echo "---- RESULT -----\n";
        print_r($result);

        echo "---- FILES ------\n";
        print_r($_FILES);

        echo "---- EVENT ------\n";
        print_r($event);

        $dump = ob_get_contents();
        ob_end_clean();

        file_put_contents(\Yii::$app->basePath."/runtime/cli/".date("d.m.Y_H:i:s_").$event->controller->id.".".$event->controller->action->id.".log",$dump);

        return $result;

    }


}