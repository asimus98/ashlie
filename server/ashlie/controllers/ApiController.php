<?php
/**
 * @project: www
 * @author Marsel Salimkhanov 2016
 * http://asimuss.ru
 */

namespace Ashlie\Controllers;

use Ashlie\Helpers\AccessToken;
use Ashlie\Helpers\Captcha;
use yii\web\Controller;
use yii\web\Response;

class ApiController extends Controller {

    /**TODO: Переименовать переменные. Неясно за что отвечают. */
    public $access_token;
    public $post;

    public $User;

    private $noAuthMethods = [
        'auth/login',
        'auth/signup',
        'auth/restore',
        'method/check',
        'captcha/get',
        'captcha/code',
        'captcha/cat',
    ];

    private $CaptchaMethods = [
        'auth/login',
        'auth/signup',
        'auth/restore'
    ];


    public function beforeAction($event){


        if(!parent::beforeAction($event)){
            return false;
        }

        \Yii::$app->response->format = Response::FORMAT_JSON;
        
        // Check Captcha
        $captcha = \Yii::$app->request->post('captcha');
        if(isset($captcha)){
            $Captcha = new Captcha;
            $check = $Captcha->check($captcha);
            if(!$check){
                $msg = $Captcha->get();
                \Yii::$app->end(json_encode($msg));
            }
        }

        if(in_array($event->controller->id."/".$event->controller->action->id,$this->CaptchaMethods)){
            $Captcha = new Captcha;
            if(!$Captcha->action($event)){
                $msg = $Captcha->get();
                \Yii::$app->end(json_encode($msg));
            }
        }

        $this->access_token = \Yii::$app->request->get('access_token');

        $this->post = \Yii::$app->request->post();

        $this->User = AccessToken::validate($this->access_token);
        if(!$this->User && !in_array($event->controller->id."/".$event->controller->action->id,$this->noAuthMethods)){
            $msg = $this->actionIndex();
            \Yii::$app->end(json_encode($msg));
        }

        return true;
    }

    public function actionIndex(){
        return [
            'code' => 401,
            'result' =>[
                'error' => [
                    'message' => 'Invalid token'
                ]
            ]
        ];
    }

    public function actionError(){
        return [
            'code' => 404,
            'result' =>[
                'error' => [
                    'message' => 'Page not Found'
                ]
            ]
        ];
    }


    public function afterAction($event,$result){

        $log_path = \Yii::$app->basePath."/runtime/api/";
        if(!file_exists($log_path) || !is_dir($log_path)) mkdir($log_path);

        ob_start();
        echo "---- REQUEST ----\n";
        print_r($_REQUEST);

        echo "---- RESULT -----\n";
        print_r($result);

        echo "---- FILES ------\n";
        print_r($_FILES);

        $dump = ob_get_contents();
        ob_end_clean();

        file_put_contents(\Yii::$app->basePath."/runtime/api/".date("d.m.Y_H:i:s_").$event->controller->id.".".$event->controller->action->id.".log",$dump);

        return $result;
    }


}