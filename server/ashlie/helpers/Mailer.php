<?php
namespace Ashlie\Helpers;

class Mailer {
	public $to;
	public $sub;
	public $msg;
	public $headers = "From: Ashlie Team<'support@ashlie.io'>\r\nContent-type: text/plain; charset=utf-8 \r\n";

	public function send(){
        mail($this->to, $this->sub, $this->msg, $this->headers);
	}
}