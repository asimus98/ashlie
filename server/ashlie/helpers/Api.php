<?php
/**
 * @project: Ashlie
 * @author Marsel Salimkhanov 2016
 * @date: ******
 */

namespace Ashlie\Helpers;

use yii\validators\EmailValidator;
use Ashlie\Api\Models\Telegram;
use Ashlie\Api\Models\Keywords;
use Ashlie\Api\Models\User;
use Ashlie\Api\Models\Token;
use Ashlie\Api\Models\Rss;

class Api {

    public function success($msg){

        if(is_array($msg)){
            return [
                'code' => 200,
                'result' =>[ 
                    'success' => $msg
                ]
            ]; 
        }

        return [
            'code' => 200,
            'result' =>[ 
                'success' => [
                    'message' => Lang::translate($msg)
                ]
            ]
        ]; 
    }

    public function error($msg, $code=400){
        if(is_array($msg)){
            return [
                'code' => $code,
                'result' =>[ 
                    'error' => $msg
                ]
            ]; 
        }

        return [
            'code' => $code,
            'result' =>[ 
                'error' => [
                    'message' => Lang::translate($msg)
                ]
            ]
        ]; 
    }

    public $post;

    // API authentication
    public function login($data){
        $email = !empty($data['email']) ? $data['email'] : false;
        $password = !empty($data['password']) ? $data['password'] : false;

        if(!$email OR !$password) return $this->error('EMPTY_FIELDS',400);

        $validator = new EmailValidator();
        if(!$validator->validate($email)) return $this->error('ERROR_EMAIL',400);

        $User = User::byEmail($email);
        if(!$User || !$User->validatePassword($password)){
            return $this->error('WRONG_AUTH_DATA',401);
        }

        $getToken = Token::findOne(['userID' => $User->id]);
        if($getToken) $getToken->delete();

        $Token = new Token;
        $Token->userID = $User->id;
        $Token->access_token($User);
        $Token->timestamp = time()+3600*24;

        $Token->save();

        $User->access_token = $Token->access_token;
        if(!$User->save()) return $this->error('ERROR_AUTH', 401);

        if(\Yii::$app->user->login($User, 3600*24)){
            return $this->success([
                'message' => Lang::translate('SUCCESS_AUTH'),
                'access_token' => $User->access_token
            ]);
        }

    }

    // API Registration
    public function signup($data){

        $email = !empty($data['email']) ? $data['email'] : false;       
        $password = !empty($data['password']) ? $data['password'] : false;

        if(!$email || !$password){
            return $this->error('EMPTY_FIELDS',400);
        }
        

        $validator = new EmailValidator();
        if(!$validator->validate($email)) 
            return $this->error('ERROR_EMAIL',400);

        $User = User::byEmail($email);
        if($User) return $this->error('ERROR_ISSET',400);

        $User = new User;
        $User->email = $email;
        $User->setPassword($password);
        $User->ip_history = json_encode([
            $_SERVER['REMOTE_ADDR']
        ]);
        $User->code = strtoupper(\Yii::$app->security->generateRandomString(5));
        $User->generateAuthKey();

        $Token = new Token;
        $Token->access_token($User);
        
        $User->access_token = $Token->access_token;
        
        if(!$User->save()) 
            return $this->error('ERROR_REGISTRATION',500);

        $Token->userID = $User->id;
        $Token->timestamp = time()+3600*24;
        $Token->save();

        if(\Yii::$app->user->login($User))
            return $this->success([
                'message' => Lang::translate('SUCCESS_REGISTARTION'),
                'access_token' => $User->access_token
            ]);
    }

    public function restore($post){
        $email = !empty($post['email']) ? $post['email'] : fasle;

        if(!$email)
            return $this->error('ERROR_EMPTY_EMAIL', 400);

        $validator = new EmailValidator();
        if(!$validator->validate($email)) 
            return $this->error('ERROR_EMAIL',400);

        $User = User::findOne(['email' => $email]);
        if(!$User)
            return $this->error('ERROR_USER_NOT_FOUND',400);

        $password = '';
        for ($i = 0; $i < 8; $i++){
            $chr = [mt_rand(65, 90),mt_rand(97, 122),mt_rand(48, 57)];
            $rand = array_rand($chr);
            $password .= chr($chr[$rand]);
        }

        $User->setPassword($password);
        if(!$User->save())
            return $this->error('ERROR_RESTORE_PASSWORD',400);

        $mailer = \Yii::$app->mailer;
        $mailer->to = $email;
        $mailer->sub = Lang::translate('RESTORE_PASSWORD');
        $mailer->msg = Lang::translate('MSG_RESTORE_PASSWORD').$password;
        $mailer->send();

        return $this->success('SUCCESS_RESTORE_PASSWORD');
    }
    
    // API RSS List
    public function rssList($User){
        $Rss = Rss::find()->where(['userID' => $User[0]->userID])->all();
        $list = new \stdClass();
        $i = 0;
        foreach ($Rss as $rss) {
            $url = new \stdClass();
            $url->query = $rss->name;
            $url->id = $rss->id;
            $url->url = $rss->feeds;

            $list->list[] = json_encode($url);
            $i++;                 
        }

        return $list;
    }

    // API Telegram List
    public function telegramList($User){
        $Telegram = Telegram::find()->where(['userID' => $User[0]->userID])->all();
        
        $list = new \stdClass();
        foreach ($Telegram as $channel) {
            $url = new \stdClass();
            $url->id = $channel->id;
            $url->title = $channel->title;

            $list->list[] = json_encode($url);
        }

        return $list;
    }

    public function telegramAdd($User){
        $channel = !empty($this->post['channel']) ? $this->post['channel'] : false;
        $title = !empty($this->post['title']) ? $this->post['title'] : false;
        if(!$channel) 
            return $this->error('EMPTY_TELEGRAM_URL'); 

        if(!$title) 
            return $this->error('EMPTY_TELEGRAM_TITLE');

        $Telegram = Telegram::findOne(['channel' => $channel]);
        if($Telegram) 
            return $this->error('ISSET_TELEGRAM_CHANNEL');

        $Telegram = new Telegram;
        $Telegram->userID = $User[0]->userID;
        $Telegram->channel = $channel;
        $Telegram->title = $title;

        if(!$Telegram->save())
            return $this->error('ERROR');

        return $this->success('SUCCESS_ADDED_TELEGRAM');

    }

    // API add RSS channel search
    public function rssAdd($User){
        $url = !empty($this->post['url']) ? $this->post['url'] : false;
        $interval = !empty($this->post['interval']) ? (int)$this->post['interval'] : 5;
        $keywords = !empty($this->post['keywords']) ? self::keywords($this->post['keywords'],$User[0]->userID) : 0;
        $channel = !empty($this->post['telegram']) ? json_encode(explode(',', $this->post['telegram'])) : false;

        if(!$url) 
            return $this->error('EMPTY_RSS_URL');

        if(!$channel) 
            return $this->error('NO_SELECT_TELEGRAM');
        
        $url = str_replace('http://', '', $url);
        $url = str_replace('https://', '', $url);
        $first = "google.feeds.Feed.FindRawCompletion('0',";
        $two = ", 200, null, 200)";
        $rss = file_get_contents('http://www.google.com/uds/GfindFeeds?callback=google.feeds.Feed.FindRawCompletion&context=0&hl=ru&q='.$url.'&key=ABQIAAAA6C4bndUCBastUbawfhKGURTFnqBuwPowtiyJohQxh-8vJXk-MBTetbTPnQAbLgs9lUkeE34hNbC15Q&v=1.0&nocache=1455733392889');
        preg_match_all ("/{(.*)}/",  $rss, $json);
  
        $rss_feeds = json_decode($json[0][0]);

        foreach ($rss_feeds->entries as $rss) {
            $Rss = new Rss;
            $Rss->userID = $User[0]->userID;
            $Rss->feeds = $rss->url;
            $Rss->name = $url;
            $Rss->timestamp = time() + ($interval*60);
            $Rss->interval = $interval;
            $Rss->type = 'urls';
            $Rss->telegram = $channel;
            $Rss->keywords = $keywords;

            if(!$Rss->save()) 
                return $this->error('ERROR_ADDED_RSS_SITE');           
        }
       
        return $this->success('SUCCESS_ADDED_RSS_SITE');


    }

    public static function keywords($keywords,$userID){
        $keywords = explode(',', $keywords);
        
        foreach ($keywords as $key) {
            $Key = new Keywords;
            $Key->userID = $userID;
            $Key->keyword = trim($key);
            $Key->save();

            $keyword[] = $Key->id;
        }

        return json_encode($keyword);
    }
    
    // API add RSS channel URL
    public function channelAdd($User){
        $url = !empty($this->post['url']) ? $this->post['url'] : false;
        $keywords = !empty($this->post['keywords']) ? self::keywords($this->post['keywords'],$User[0]->userID) : 0;
        $channel = !empty($this->post['telegram']) ? json_encode(explode(',', $this->post['telegram'])) : false;
        $interval = !empty($this->post['interval']) ? (int)$this->post['interval'] : 5;

        if(!$url) 
            return $this->error('EMPTY_RSS_URL_CHANNEL');

        if(!$channel) 
            return $this->error('NO_SELECT_TELEGRAM');

        $name = str_replace('http://', '', $url);
        $name = str_replace('https://', '', $name);

        $findRss = Rss::findOne(['name' => $name, 'userID' => $User[0]->userID]);
        if($findRss) 
            return $this->error('ERROR_ISSET_RSS_CHANNEL');

        $request = $this->request($url);

        if($request['http'] != '200') 
            return $this->error('ERROR_RSS_CHANNEL_URL');

        if(!simplexml_load_string($request['xml'])) 
            return $this->error('ERROR_RSS_CHANNEL_URL');
        
        $Rss = new Rss;
        $Rss->userID = $User[0]->userID;
        $Rss->feeds = $url;
        $Rss->name = $name;
        $Rss->timestamp = time() + ($interval*60);
        $Rss->interval = $interval;
        $Rss->type = 'url';
        $Rss->telegram = $channel;
        $Rss->keywords = $keywords;

        if(!$Rss->save()) 
            return $this->error('ERROR_ADDED_RSS_SITE');

        return $this->success('SUCCESS_ADDED_RSS_CHANNEL');

    }


    public function rssRemove($User){
        $id = !empty($this->post['id']) ? $this->post['id'] : false;

        if(!$id) 
            return $this->error('EMPTY_FIELDS');

        $Rss = Rss::findOne(['id' => $id]);
        if(!$Rss) 
            return $this->error('ERROR_RSS_NOT_FOUND');

        if($Rss->userID != $User[0]->userID) 
            return $this->error('ACCESS_DENIED');

        if(!$Rss->delete()) 
            return $this->error('ERROR_REMOVE_RSS');

        return $this->success('SUCCESS_REMOVE_RSS');
    } 

    public function rssFind($User){
        $url = !empty($this->post['url']) ? $this->post['url'] : false;

        $url = str_replace('http://', '', $url);
        $url = str_replace('https://', '', $url);

        if(!$url) 
            return $this->error('EMPTY_RSS_URL_CHANNEL');

        $Rss = Rss::find()->where(['like', 'feeds', $url])
                          ->orWhere(['like', 'name', $url])
                          ->all();

        foreach ($Rss as $rss) {
            $rssFeed = new \stdClass();
            $rssFeed->name = $rss->name;
            $rssFeed->url = $rss->feeds;

            $rssList['items'][] = $rssFeed;
        }

        return $this->success($rssList);
    }
    

    public function request($url){
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
        $response = curl_exec($curl);

        $http = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);
            
        return ['http' => $http,'xml'=> $response];
    }

}