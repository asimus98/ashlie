<?php
/**
 * @project: Ashlie
 * @author Marsel Salimkhanov 2016
 * @date: ******
 */

namespace Ashlie\Helpers;

use Ashlie\Api\Models\Token;

class AccessToken {

    public static function validate($token){
        if(empty($token)) return false;

        $Token = Token::find()
            ->where(['access_token' => $token])
            ->andWhere(['>', 'timestamp', time()])
            ->all();

        if(!$Token) return false;
        else return $Token;
    }
}
