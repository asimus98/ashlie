<?php
/**
 * @project: www
 * @author Marsel Salimkhanov 2016
 * http://asimuss.ru
 */

namespace Ashlie\Helpers;
use Ashlie\Api\Models\Captcha as CaptchaDB;
use Ashlie\Api\Models\Actions;


class Captcha {
	public $ip;

	public function get(){
		$url = substr(md5(time()), 0, rand(100, 150));
		$session = \Yii::$app->session;

		$newCode = "";
		for ($i = 0; $i < 6; $i++)
		    $newCode .= chr(mt_rand(97, 122));

		$captcha = $session->get('captcha');
		if($captcha){
			$session->remove('captcha');
			$session->set('captcha',$newCode);
		}else{
			$session->set('captcha',$newCode);
		}

	    return [
	        'result' => [
	            'error' => [
	                'message' => Lang::translate('ERROR_ENTER_CAPTCHA')
	            ],
	            'captcha' => '/v1/method/captcha.get'
	        ]
	    ];
	}

	public function action($event){
		$session = \Yii::$app->session;
		$action = $event->controller->id."/".$event->controller->action->id;

		$getAction = $session->get('action');
		$decodeAction = json_decode($getAction);

		if(!$getAction OR $decodeAction->action != $action){
			$Action = new \stdClass();
			$Action->request = 1;
			$Action->action = $action;

			$session->set('action',json_encode($Action));
			
			return true;
		}

		$request = $decodeAction->request;

		if($request == 5){
			return false;
		}else{
			$decodeAction->request += 1;

			$session->remove('action');
			$session->set('action',json_encode($decodeAction));
		}

		return true;
	}

	public function check($code){
		$session = \Yii::$app->session;
		$captcha = $session->get('captcha');
		if(!$captcha)
			return true;

		if($captcha == $code){
			$session->remove('action');
			$session->remove('captcha');
			return true;
		}

		return false;
	}
}