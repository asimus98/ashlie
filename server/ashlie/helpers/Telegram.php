<?php
/**
 * @project: Ashlie
 * @author Marsel Salimkhanov 2016
 * @date: ******
 */

namespace Ashlie\Helpers;

use Ashlie\Models\User;

class Telegram {

    /**
    * Telegram Bot access token и URL.
    */
    private $access_token = '188709105:AAHTV38qCr1OUrro9AdM0P_p1ngCmD0-2HM';
    private $api = 'https://api.telegram.org/bot';


    public function create($chat_id){
        $Telegram = new Telegram;

        $Telegram->chat_id = $chat_id;
        $Telegram->status = 0;

        $Telegram->save();
        return $Telegram;

    }

    public function byChat($chat_id){
       return Telegram::findOne(['chat_id' => $chat_id]);

    }

    public function send($chat_id, $msg){
        $url = $this->api.$this->access_token.'/sendMessage?chat_id=' . $chat_id . '&text=' . urlencode($msg);

        $request = curl_init();
        curl_setopt($request, CURLOPT_URL, $url);
        curl_setopt($request, CURLOPT_RETURNTRANSFER,true);
        $response = curl_exec($request);

        curl_close($request);
    }

    public function login($email,$chat_id){
        $User = User::byEmail($email);
        if(!$User) return 'Пользователь с таким email не найден';

        $Telegram = Telegram::findOne(['chat_id' => $chat_id]);
        $Telegram->userID = $User->id;
        $Telegram->status = '1';

        if($Telegram->save()){      
            return 'Введите пароль';
        }
    }

    public function password($password,$chat_id){
        $Telegram = Telegram::findOne(['chat_id' => $chat_id]);
        $User = User::byID($Telegram->userID);
        if(!$User || !$User->validatePassword($password)) return 'Неверный пароль';

        $Telegram->status = '2';

        if($Telegram->save()){      
            return 'Успешно!';
        }
    }
}
