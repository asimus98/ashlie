<?php
/**
 * @project: Ashlie
 * @author Marsel Salimkhanov 2016
 * @date: ******
 */

namespace Ashlie\Helpers;

use Yii;

class Lang {


    public static function translate($constant){

        $lang = !empty(\Yii::$app->language) ? Yii::$app->language : "ru-RU";
        $translations = include dirname(__DIR__)."/language/$lang.php";
        $result = !empty($translations[$constant]) ? $translations[$constant] : $translations['UNDEFINED'];
        return $result;

    }

}