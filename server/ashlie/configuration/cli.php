<?php
/**
 * @project: www
 * @author Marsel Salimkhanov 2016
 * http://asimuss.ru
 */

return [
    'id' => 'daemon',
    'controllerNamespace' => 'Ashlie\Cli\Controllers',
    'defaultRoute' => 'feed',
    'components' => []
];