<?php
/**
 * @project: www
 * @author Marsel Salimkhanov 2016
 * http://asimuss.ru
 */

return [
    'id' => 'Ashlie',
    'name' => 'Ashlie Project',
    'controllerNamespace' => 'Ashlie\Api\Controllers',
    'defaultRoute' => 'method',
    'components' => [
        'errorHandler'=>array(
            'errorAction'=>'method/error',
        ),
        'request' => [
            'cookieValidationKey' => 'zOQ89pJF04kTXGxm4OSSSt_-9DHT0Us_',
            'enableCsrfValidation' => false,
        ],
        'user' => [
            'identityClass' => 'Ashlie\Models\User'
        ],
        'urlManager' => [
            'rules' => [
                '/v1/method/telegram.addChannel' => 'telegram/add',
                '/v1/method/telegram.getList' => 'telegram/list',
                '/v1/method/rss.addSite' => 'rss/add',
                '/v1/method/rss.addChannel' => 'rss/channel',
                '/v1/method/<controller>.<action>' => '<controller>/<action>',
                '/v1/method/<controller>' => '<controller>/index',
                '/' => 'api/index',
            ]
        ],
        'mailer' => [
          'class' => 'Ashlie\Helpers\Mailer',
        ],
    ],
    'controllerMap' => []
];