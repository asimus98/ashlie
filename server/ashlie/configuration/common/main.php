<?php
/**
 * @project: Ashlie
 * @author Marsel Salimkhanov 2016
 * @date: ******
 */
return [
    'basePath' => dirname(dirname(dirname(__DIR__))),
    'vendorPath' => dirname(dirname(dirname(__DIR__))) . '/vendor',
    'bootstrap' => ['log'],
    "components" => [
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
    ],
    'timeZone' => 'Europe/Moscow',
    'charset' => 'UTF-8',
    'language' => 'ru-RU',
    'params' => [
        'adminEmail' => 'support@bradius.ru',
        'supportEmail' => 'support@bradius.ru',
        'user.passwordResetTokenExpire' => 3600,
        'email' => [
            'team@ashlie.io' => 'Ashlie Team'
        ]
    ],
];