<?php
/**
 * @project: Ashlie
 * @author Marsel Salimkhanov 2016
 * @date: ******
 */

Yii::setAlias('ashlie', dirname(dirname(__DIR__)) . '/ashlie');
Yii::setAlias('api', dirname(dirname(__DIR__)) . '/api');
Yii::setAlias('daemon', dirname(dirname(__DIR__)) . '/daemon');