<?php
/**
 * @project: Ashlie
 * @author Marsel Salimkhanov 2016
 * @date: ******
 */

namespace Ashlie\Api\Models;

use yii\web\IdentityInterface;
use Ashlie\Models\Ashlie as Model;

class User extends Model implements IdentityInterface{

    /**
     * Search
     */

    public static function byEmail($email){
        return static::findOne([
            'email' => $email
        ]);
    }

    public static function byTooligram($key,$user){
        return static::findOne([
            'id' => $user,
            'tooligramKey' => $key
        ]);
    }

    public static function byKey($key){
        return static::findOne([
            'authKey' => $key
        ]);
    }


    /**
     * Static
     */

    public static function current(){
        return \Yii::$app->user->identity;
    }


    /**
     * Helpers
     */

    public function setPassword($password){
        $this->password = hash('sha512',$password);
    }

    public function generateAuthKey(){
        $key = \Yii::$app->security->generateRandomString(512);
        if(self::byKey($key)) return $this->generateAuthKey();
        $this->authKey = $key;
    }

    public function validatePassword($password){
        return hash('sha512',$password) == $this->password;
    }

    /**
     * Auth methods
     */

    public static function findIdentity($id){
        return static::findOne(
            ['id' => $id ]
        );
    }

    public static function findIdentityByAccessToken($token, $type = null) {
        return static::findOne(
            ['authKey' => $token]
        );
    }

    public function getId() {
        return $this->id;
    }

    public function getAuthKey() {
        return $this->authKey;
    }

    public function validateAuthKey($authKey) {
        return $this->authKey === $authKey;
    }

    /**
     * Table Name
     */
    public static function tableName(){
        return 'users';
    }

}