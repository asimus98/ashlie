<?php
/**
 * @project: Ashlie
 * @author Marsel Salimkhanov 2016
 * @date: ******
 */

namespace Ashlie\Api\Models;

use Ashlie\Models\Ashlie as Model;

class Token extends Model{

	public function access_token($User){
		$this->access_token = hash('sha256', $User->email.$User->password.time().rand(1000000, 100000000000));
	}

    /**
     * Table Name
     */
    public static function tableName(){
        return 'token';
    }

}