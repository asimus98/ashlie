<?php
/**
 * @project: Ashlie
 * @author Marsel Salimkhanov 2016
 * @date: ******
 */

namespace Ashlie\Api\Models;

use Ashlie\Models\Ashlie as Model;

class Keywords extends Model{

    /**
     * Table Name
     */
    public static function tableName(){
        return 'keywords';
    }

}