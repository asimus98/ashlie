<?php
/**
 * @project: Ashlie
 * @author Marsel Salimkhanov 2016
 * @date: ******
 */

defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'dev');

require(dirname(dirname(__DIR__)) . '/vendor/autoload.php');
require(dirname(dirname(__DIR__)) . '/vendor/yiisoft/yii2/Yii.php');
require(dirname(dirname(__DIR__)) . '/ashlie/configuration/common/bootstrap.php');

$config = \yii\helpers\ArrayHelper::merge(
    require dirname(dirname(__DIR__)) . "/ashlie/configuration/common/main.php",
    require dirname(dirname(__DIR__)) . "/ashlie/configuration/common/db.php",
    require dirname(dirname(__DIR__)) . "/ashlie/configuration/api.php"
);

$app = new yii\web\Application($config);
$app->run();