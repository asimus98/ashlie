<?php
/**
 * @project: Ashlie
 * @author Marsel Salimkhanov 2016
 * @date: ******
 */

namespace Ashlie\Api\Controllers;

use Ashlie\Controllers\ApiController as Controller;
use Ashlie\Helpers\Telegram;

class TelegramController extends Controller {

    /**
	 * WebHook
	*/
	public $output = json_decode(file_get_contents('php://input'), TRUE);
    
    public function beforeAction($action){
        
        if(!parent::beforeAction($action)){
            return false;
        }

        return true;
    }

    public function actionIndex(){
    	$chat_id = $this->output['message']['chat']['id'];
        $first_name = $this->output['message']['chat']['first_name'];
        $message = mb_strtolower($this->output['message']['text']);

        $Telegram = Telegram::byChat($chat_id);

        if($message == '/start'){

        	if(!$Telegram){
                $Telegram = Telegram::create($chat_id);
        	}

        	if($Telegram->status == '0'){
        		$msg = 'Введите email зарегистрированный на сайте';
        	}elseif ($Telegram->status == '1') {
        		$msg = 'Введите пароль';
        	}else{
        		$msg = 'Успешно!';
        	}

        	$send = Telegram::send($chat_id,$msg);        	
        }

        if($Telegram->status == '0'){
        	// Check email
        	$login = Telegram::login($message,$chat_id);
        	$msg = $login;
        }elseif ($Telegram->status == '1') {
        	// Check password
        	$password = Telegram::password($message, $chat_id);
        	$msg = $password;
        }else{
        	$msg = 'Успешно!';
        }

        $send = Telegram::send($chat_id,$msg);  
        
    }

}


