<?php
/**
 * @project: Ashlie
 * @author Marsel Salimkhanov 2016
 * @date: ******
 */

namespace Ashlie\Api\Controllers;

use Ashlie\Controllers\ApiController as Controller;
use Ashlie\Helpers\Api;

class RssController extends Controller {

    public function actionChannel(){
        $Api = new Api;
        $Api->post = $this->post;
        return $Api->channelAdd($this->User);
    }

    public function actionRemove(){
        $Api = new Api;
        $Api->post = $this->post;
        return $Api->rssRemove($this->User);
    }

    public function actionList(){
        $Api = new Api;
        $Api->post = $this->post;
        return $Api->rssList($this->User);
    }

    public function actionAdd(){
        $Api = new Api;
        $Api->post = $this->post;
        return $Api->rssAdd($this->User);
    }

    public function actionFind(){
        $Api = new Api;
        $Api->post = $this->post;
        return $Api->rssFind($this->User);
    }

}


