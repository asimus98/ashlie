<?php
/**
 * @project: Ashlie
 * @author Marsel Salimkhanov 2016
 * @date: ******
 */

namespace Ashlie\Api\Controllers;

use Ashlie\Controllers\ApiController as Controller;
use Ashlie\Helpers\Api;
use yii\web\Response;
use Ashlie\Api\Models\Captcha;
use Ashlie\Helpers\Captcha as newCaptcha;
use yii\captcha\CaptchaAction;

class CaptchaController extends Controller {

    public function actionGet(){

        $session = \Yii::$app->session;
        $Api = new Api;

        $captcha = $session->get('captcha');
        if(!$captcha){
            $Captcha = new newCaptcha;
            $Captcha->get();
        }


        $renderCaptcha = new CaptchaAction('get', 'captcha');
        $renderCaptcha->fixedVerifyCode = $session->get('captcha');
        
        return $renderCaptcha->run();

        //echo substr(md5(time()), 0, 100);        
    }

    public function actionCat(){
       
        $password = '';
        for ($i = 0; $i < 8; $i++){
            $chr = [mt_rand(65, 90),mt_rand(97, 122),mt_rand(48, 57)];
            $rand = array_rand($chr);
            $password .= chr($chr[$rand]);
        }

        echo $password;
    }

}


