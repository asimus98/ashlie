<?php
/**
 * @project: Ashlie
 * @author Marsel Salimkhanov 2016
 * @date: ******
 */

namespace Ashlie\Api\Controllers;

use Ashlie\Controllers\ApiController as Controller;
use Ashlie\Helpers\Api;

class AuthController extends Controller {

    public $post;

    public function actionLogin(){
        $Api = new Api;
        return $Api->login($this->post);
    }

    public function actionSignup(){
        $Api = new Api;
        return $Api->signup($this->post);
    }

    public function actionRestore(){
        $Api = new Api;
        return $Api->restore($this->post);
    }

}


