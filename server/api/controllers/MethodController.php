<?php
/**
 * @project: Ashlie
 * @author Marsel Salimkhanov 2016
 * @date: ******
 */

namespace Ashlie\Api\Controllers;

use Ashlie\Controllers\ApiController as Controller;
use Ashlie\Helpers\Api;

class MethodController extends Controller {


    public function actionRssadd(){
        $Api = new Api;
        $Api->post = $this->post;
        return $Api->rssAdd($this->Access);
    }

    public function actionCheck(){
        return [
            'message' => 'Silence is golden.'
        ];
    }

    public function actionTelegramadd(){
        $Api = new Api;
        $Api->post = $this->post;
        return $Api->telegramAdd($this->User);
    }

    public function actionTelegramlist(){
        $Api = new Api;
        $Api->post = $this->post;
        return $Api->telegramList($this->User);
    }

    public function actionChannel(){
        $Api = new Api;
        $Api->post = $this->post;
        return $Api->channelAdd($this->User);
    }

    public function actionRssremove(){
        $Api = new Api;
        $Api->post = $this->post;
        return $Api->rssRemove($this->User);
    }

}


