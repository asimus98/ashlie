<?php
/**
 * @project: Ashlie
 * @author Marsel Salimkhanov 2016
 * @date: ******
 */

namespace Ashlie\Api\Controllers;

use Ashlie\Controllers\ApiController as Controller;
use Ashlie\Helpers\Api;

class TelegramController extends Controller {

    public function actionAdd(){
        $Api = new Api;
        $Api->post = $this->post;
        return $Api->telegramAdd($this->User);
    }

    public function actionCapt(){
        
    }

    public function actionList(){
        $Api = new Api;
        $Api->post = $this->post;
        return $Api->telegramList($this->User);
    }



}


